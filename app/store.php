<?php

namespace App;
use App\User;
use App\withdrawal;
use App\Transaction;
use App\bankDetails;
use Illuminate\Database\Eloquent\Model;

class store extends Model
{
    public function user() {

    	return $this->belongsTo(User::class);

    }

    public function withdrawals(){

        return $this->hasMany(withdrawal::class);
    }

    public function transactions(){

        return $this->hasMany(Transaction::class);
    }

    public function bankDetails(){

        return $this->belongsTo(bankDetails::class);
    }
}
