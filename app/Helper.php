<?php
namespace App;


class Helper
{
    
    /**
     * @param $data
     * @param string $message 
     * @param bool $status
     * @return \Illuminate\Http\JsonResponse
     */
    public static function responseJson($data = null, string $message = null, int $code){
        $body = [
            
            'message' => $message
        ];

        if(!is_null($data)){
            $body['status'] = true;
            $body['data'] = $data;
        }
        else{
           $body['status'] = false;
        }

        return response()->json($body,$code);
    }

    /**
     * @param $validator
     * @return \Illuminate\Http\JsonResponse
     */
    public static function invalidRequest($errors,$code = 400){

        return self::responseJson(null, $errors, $code);
    }

    public static function validRequest($data, $code = 200){
        $message = 'OK';
       
        return self::responseJson($data, $message, 200);
    }
}