<?php

namespace App;

use App\Bank;
use App\store;
use Illuminate\Database\Eloquent\Model;

class bankDetails extends Model
{
    public function bank(){
    	return $this->belongsTo(Bank::class);
    }

    public function store() {

    	return $this->belongsTo(store::class);

    }
}
