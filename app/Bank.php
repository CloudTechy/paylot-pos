<?php

namespace App;

use App\bankDetails;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    public function bankDetails(){
    	return $this->hasMany(bankDetails::class);
    }
}
