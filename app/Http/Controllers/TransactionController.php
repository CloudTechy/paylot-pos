<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Transaction;
use App\store;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\storeTransactionRequest;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * create a transaction
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * form parameters required are: int $store_id, int $amount, string $reference[optional], string $customer_name
     * returns string transaction->id
     */
    public function store(storeTransactionRequest $request)
    {

        $validated = $request->validated();
         try {

        $transaction = Transaction::create($validated);

        }catch(\Exception $bug){

            return $this->exception($bug);
        }

        return Helper::validRequest($transaction->id,200);
    }

     
    /**
     * Display the specified transaction resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show($transaction)
    {
         try {
          
            $transaction = Transaction::with('store.User.kk')->find($transaction);

        }catch(\Exception $bug){

            return $this->exception($bug);
        }
        
       return Helper::validRequest($transaction,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
