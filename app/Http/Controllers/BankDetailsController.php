<?php

namespace App\Http\Controllers;

use App\bankDetails;
use Illuminate\Http\Request;

class BankDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\bankDetails  $bankDetails
     * @return \Illuminate\Http\Response
     */
    public function show(bankDetails $bankDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\bankDetails  $bankDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(bankDetails $bankDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\bankDetails  $bankDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, bankDetails $bankDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\bankDetails  $bankDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(bankDetails $bankDetails)
    {
        //
    }
}
