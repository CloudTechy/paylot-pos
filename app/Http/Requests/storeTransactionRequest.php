<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class storeTransactionRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *form parameters to validate are: int $store_id, int $amount, string $reference[optional], string $customer_name
     * @return array
     */
    public function rules()
    {
        return [
            'store_id' => 'bail|numeric|required',
            'amount' => 'bail|numeric|required',
            'reference' =>'bail|nullable|string',
            'customer_name' => 'required|string|min:4',
            'sent' => 'nullable'
        ];
    }
}
