<?php

namespace App;
use App\store;
use Illuminate\Database\Eloquent\Model;
use \BinaryCabin\LaravelUUID\Traits\HasUUID;

class Transaction extends Model
{
    use HasUUID;
    protected $uuidFieldName = 'id';
   	public $incrementing = false;
	protected $keyType = 'string';

     protected $fillable = [
        'amount', 'store_id', 'reference','customer_name',
    ];


    public function store(){

        return $this->belongsTo(store::class);
    }
}
