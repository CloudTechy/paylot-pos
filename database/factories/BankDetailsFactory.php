<?php

use Faker\Generator as Faker;

$factory->define(App\bankDetails::class, function (Faker $faker) {
    return [
        'bank_id' =>function() {

        		return factory(App\Bank::class)->create()->id;
        },
        'AccountName' => $faker->name,
        'AccountNumber' => $faker->bankAccountNumber,
    ];
});
