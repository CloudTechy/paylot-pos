<?php

use Faker\Generator as Faker;

$factory->define(App\Transaction::class, function (Faker $faker) {
    return [
    	'store_id' => $faker->numberBetween(1,5),
        'amount' => $faker->randomNumber(5),
        'sent' => $faker->boolean,
        'confirmed' => $faker->boolean,
        'reference' => $faker->uuid,
        'customer_name'=> $faker->name,
    ];
});
