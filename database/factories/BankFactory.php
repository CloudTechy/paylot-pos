<?php

use Faker\Generator as Faker;

$factory->define(App\Bank::class, function (Faker $faker) {
	$banks = ['GTBank','Access','Diamond','Polaris','UBA','First Bank'];
    return [
        'Name' => $banks[array_rand($banks)],
    ];
});