<?php

use Faker\Generator as Faker;

$factory->define(App\store::class, function (Faker $faker) {
    return [

    	'Name' => $faker->company,
    	'address' => $faker->address,
    	'phoneNumber' => $faker->phoneNumber,
    	'bank_details_id' => $faker->numberBetween(1,5),
    	'user_id' => $faker->numberBetween(1,5),
    	'pendingBalance' => $faker->randomNumber(5),

    ];
});
