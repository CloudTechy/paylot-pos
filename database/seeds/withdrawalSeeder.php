<?php

use Illuminate\Database\Seeder;

class withdrawalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\withdrawal::class,5)->create();
    }
}
