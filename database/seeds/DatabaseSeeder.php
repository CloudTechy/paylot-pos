<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(withdrawalSeeder::class);
         $this->call(TransactionTableSeeder::class);
         $this->call(StoreTableSeeder::class);
         //$this->call(BankTableSeeder::class);
         $this->call(BankDetailsTableSeeder::class);
    }
}
