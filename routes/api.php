<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| API Routes for all transaction resources
|--------------------------------------------------------------------------
*/
Route::post('/transaction', 'transactionController@store');
Route::get('/transaction/{transaction}', 'transactionController@show');

/*
|--------------------------------------------------------------------------
| API Routes for all user resources
|--------------------------------------------------------------------------
*/
Auth::routes(['verify' => true]);
//Route::post('register', 'Auth\RegisterController@register');